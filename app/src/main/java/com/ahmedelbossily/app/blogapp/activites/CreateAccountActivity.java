package com.ahmedelbossily.app.blogapp.activites;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ahmedelbossily.app.blogapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateAccountActivity extends AppCompatActivity {

    private EditText emailAct;
    private EditText passwordAct;
    private Button createAccountAct;
    private DatabaseReference mDatabaseReference;
    private FirebaseDatabase mDatabase;
    private FirebaseAuth mAuth;
    private ProgressDialog mProgressDialog;
    Boolean autenticazione = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        emailAct = findViewById(R.id.emailAct);
        passwordAct = findViewById(R.id.passwordAct);
        createAccountAct = findViewById(R.id.createAccountAct);

        mDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mDatabase.getReference().child("Users");
        mAuth = FirebaseAuth.getInstance();
        mProgressDialog = new ProgressDialog(this);

        createAccountAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewAccount();
            }
        });
    }

    private void createNewAccount() {
        final String Email = emailAct.getText().toString();
        final String Password = passwordAct.getText().toString().trim();

        if (verifyData(Email, Password)) {
            mAuth.createUserWithEmailAndPassword(Email, Password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    if (authResult != null) {
                        Intent intent = new Intent(CreateAccountActivity.this, PostListActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Problemi con l'autenticazione", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private boolean verifyData (String Email, String Password) {
        Boolean controllo = true;
        String emailPattern = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$");

        if (TextUtils.isEmpty(Email) || TextUtils.isEmpty(Password)) {
            Toast.makeText(this, "Email e/o password vuoti", Toast.LENGTH_SHORT).show();
            controllo = false;
        } else {
            if (Email.length() < 6 || Password.length() < 6) {
                Toast.makeText(this, "Email e/o password inferiori a 6 caratteri", Toast.LENGTH_SHORT).show();
                controllo = false;
            } else {
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(Email).matches() || !Email.matches(emailPattern)) {
                    Toast.makeText(this, "Email e/o password non validi", Toast.LENGTH_SHORT).show();
                    controllo = false;
                }
            }
        }

        if (controllo)
        {
            mAuth.fetchProvidersForEmail(Email).addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
                @Override
                public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                    boolean check = !task.getResult().getProviders().isEmpty();
                    if(check)
                    {
                        Toast.makeText(getApplicationContext(), "Utente già esistente", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        return controllo;
    }
}

package com.ahmedelbossily.app.blogapp.model;

/**
 * Created by a_anw on 11/03/2018.
 */

public class Blog {
    private String desc;
    private String m_desc;
    private String id_post;
    private String userid;
    private Boolean visible;

    public Blog() {
    }

    public Blog(String desc, String id_post, String userid, Boolean visible) {
        this.desc = desc;
        this.id_post = id_post;
        this.userid = userid;
        this.visible = visible;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getIdpost() {
        return id_post;
    }

    public void setIdpost(String id_post) {
        this.id_post = id_post;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
}

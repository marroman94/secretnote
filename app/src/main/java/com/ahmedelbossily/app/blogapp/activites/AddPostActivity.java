package com.ahmedelbossily.app.blogapp.activites;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ahmedelbossily.app.blogapp.R;
import com.ahmedelbossily.app.blogapp.model.Blog;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddPostActivity extends AppCompatActivity {

    private EditText mPostDesc;
    private Button mSubmitButton;
    private ImageButton action_signout;
    private DatabaseReference mPostDatabase;
    private String mPostId;
    private String mDescId;
    private String mUserId;
    private Boolean mVisible;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);

        mProgress = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mUserId = mUser.getUid();
        mPostDatabase = FirebaseDatabase.getInstance().getReference().child("Blog").child(mUserId);
        mPostDesc = (EditText) findViewById(R.id.descriptionEt);

        action_signout = (ImageButton) findViewById(R.id.action_signout);
        action_signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUser != null && mAuth != null) {
                    mAuth.signOut();
                    startActivity(new Intent(AddPostActivity.this, MainActivity.class));
                    finish();
                }
            }
        });


        mSubmitButton = (Button) findViewById(R.id.submitPost);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Posting to Database
                startPosting();
            }
        });
    }

    private void startPosting() {
        mProgress.setMessage("Creazione post in corso...");
        mProgress.show();

        final String descValue = mPostDesc.getText().toString().trim();
        Pattern ps = Pattern.compile("^[a-zA-Z0-9.,:;()'?!éèàùì ]+$");
        Matcher ms = ps.matcher(descValue);
        boolean controllo_car = ms.matches();

        if (!TextUtils.isEmpty(descValue))
        {
            if (controllo_car) {
                // Start the uploading...
                //Creiamo un riferimento ad un nodo figlio del database autogenerato

                mDescId = descValue;
                mPostId = mPostDatabase.push().getKey();
                mUserId = mUser.getUid();
                mVisible = true;

                Blog blog = new Blog(mDescId, mPostId, mUserId, mVisible);
                mPostDatabase.child(mPostId).setValue(blog);
                mProgress.dismiss();

                startActivity(new Intent(AddPostActivity.this, PostListActivity.class));
                finish();
            }
            else
            {
                Toast.makeText(this, "Caratteri non ammessi", Toast.LENGTH_SHORT).show();
                mProgress.dismiss();
                startActivity(new Intent(AddPostActivity.this, PostListActivity.class));
                finish();
            }
        }
        else
        {
            Toast.makeText(this, "Il post non può essere vuoto", Toast.LENGTH_SHORT).show();
            mProgress.dismiss();
            startActivity(new Intent(AddPostActivity.this, PostListActivity.class));
            finish();
        }
    }
}


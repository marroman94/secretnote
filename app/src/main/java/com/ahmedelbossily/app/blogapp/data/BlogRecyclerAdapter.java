package com.ahmedelbossily.app.blogapp.data;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmedelbossily.app.blogapp.activites.AddPostActivity;
import com.ahmedelbossily.app.blogapp.model.Blog;
import com.ahmedelbossily.app.blogapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BlogRecyclerAdapter extends RecyclerView.Adapter<BlogRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Blog> blogList;
    private ArrayAdapter<Blog> adapter;
    public int n_posizione;
    String temp,m_desc,mUserId;
    public DatabaseReference mPostDatabase;
    private FirebaseUser mUser;
    private FirebaseAuth mAuth;

    public BlogRecyclerAdapter(Context context, List<Blog> blogList) {
        this.context = context;
        this.blogList = blogList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_row, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Blog blog = blogList.get(position);
        holder.desc.setText(blog.getDesc());

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mUserId = mUser.getUid();

        if (blogList.get(position).getVisible()) {

            holder.desc.getPaint().setMaskFilter(null);

            holder.nascondi.setImageResource(R.drawable.eyes);
        }
        else
        {
            if (Build.VERSION.SDK_INT >= 11) {
                holder.desc.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }
            float radius = holder.desc.getTextSize() / 3;
            BlurMaskFilter filter = new BlurMaskFilter(radius, BlurMaskFilter.Blur.NORMAL);
            holder.desc.getPaint().setMaskFilter(filter);

            holder.nascondi.setImageResource(R.drawable.not_eyes);
        }

        holder.desc.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (blogList.get(position).getVisible()) {

                    LayoutInflater inflater = LayoutInflater.from(context);
                    final AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
                    View dialogView = inflater.inflate(R.layout.update_dialog, null);

                    Button submitPost = (Button) dialogView.findViewById(R.id.m_submitPost);
                    final EditText descriptionEt = (EditText) dialogView.findViewById(R.id.m_description);
                    descriptionEt.setText(blogList.get(position).getDesc());

                    mBuilder.setView(dialogView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();

                    submitPost.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            m_desc = descriptionEt.getText().toString();
                            Pattern ps = Pattern.compile("^[a-zA-Z0-9.,:;()'?!éèàùì ]+$");
                            Matcher ms = ps.matcher(m_desc);
                            boolean controllo_car = ms.matches();

                            if (!m_desc.isEmpty()) {
                                if (controllo_car) {
                                    Toast.makeText(context, "Post modificato", Toast.LENGTH_SHORT).show();
                                    String id_post = blog.getIdpost();
                                    DatabaseReference db = FirebaseDatabase.getInstance().getReference("Blog").child(mUserId).child(id_post);
                                    blogList.get(position).setDesc(m_desc);
                                    db.setValue(blog);
                                    notifyDataSetChanged();
                                    dialog.cancel();
                                }
                                else
                                {
                                    Toast.makeText(context, "Caratteri non ammessi", Toast.LENGTH_SHORT).show();
                                    dialog.cancel();
                                }
                            }
                            else
                            {
                                Toast.makeText(context, "Il post non può essere vuoto!", Toast.LENGTH_SHORT).show();
                                dialog.cancel();
                            }
                        }
                    });
                }
                else
                {
                    Toast.makeText(context, "Post non editabile", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (blogList.get(position).getVisible()) {
                    Toast.makeText(context, "Post eliminato", Toast.LENGTH_SHORT).show();
                    String id_post = blog.getIdpost();
                    DatabaseReference db = FirebaseDatabase.getInstance().getReference("Blog").child(mUserId).child(id_post);
                    db.removeValue();
                    blogList.remove(blog);
                    notifyDataSetChanged();
                } else
                {
                    Toast.makeText(context, "Post non cancellabile", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.nascondi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (blogList.get(position).getVisible()) {

                    if (Build.VERSION.SDK_INT >= 11) {
                        holder.desc.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    }
                    float radius = holder.desc.getTextSize() / 3;
                    BlurMaskFilter filter = new BlurMaskFilter(radius, BlurMaskFilter.Blur.NORMAL);
                    holder.desc.getPaint().setMaskFilter(filter);

                    Toast.makeText(context, "Post nascosto", Toast.LENGTH_SHORT).show();
                    String id_post = blog.getIdpost();
                    DatabaseReference db = FirebaseDatabase.getInstance().getReference("Blog").child(mUserId).child(id_post);
                    blogList.get(position).setVisible(false);
                    db.setValue(blog);
                    notifyDataSetChanged();
                } else {

                    holder.desc.getPaint().setMaskFilter(null);

                    Toast.makeText(context, "Post visibile", Toast.LENGTH_SHORT).show();
                    String id_post = blog.getIdpost();
                    DatabaseReference db = FirebaseDatabase.getInstance().getReference("Blog").child(mUserId).child(id_post);
                    blogList.get(position).setVisible(true);
                    db.setValue(blog);
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return blogList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView desc;
        String userid;
        String id_post;
        ImageButton cancel, nascondi, modifica_testo;

        public ViewHolder(final View view, Context ctx) {
            super(view);
            context = ctx;

            cancel = (ImageButton) view.findViewById(R.id.elimina_post);
            nascondi = (ImageButton) view.findViewById(R.id.nascondi_testo);
            desc = (TextView) view.findViewById(R.id.postTextList);
            userid = null;
            id_post = null;
        }
    }
}
